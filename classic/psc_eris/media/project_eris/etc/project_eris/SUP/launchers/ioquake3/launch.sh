#!/bin/sh
source "/var/volatile/project_eris.cfg"
cd "/var/volatile/launchtmp"
chmod +x "ioquake3.arm"
echo -n 2 > "/data/power/disable"

${PROJECT_ERIS_PATH}/bin/sdl_input_text_display " " 0 0 12 "/usr/share/fonts/ttf/LiberationMono-Regular.ttf" 0 0 0 "$(pwd)/quake3_controller_select.png" XO

SELECTEDOPTION=$?

if [ "${SELECTEDOPTION}" = "100" ]; then
  # Set up controller config according to only one DPAD
  sed -i -e 's/bind LEFTARROW "[^"]*"/bind LEFTARROW "+left"/g' ".q3a/baseq3/q3config.cfg"
  sed -i -e 's/bind RIGHTARROW "[^"]*"/bind RIGHTARROW "+right"/g' ".q3a/baseq3/q3config.cfg"
elif [ "${SELECTEDOPTION}" = "101" ]; then
  # Set up controller config as we have analouge sticks
  sed -i -e 's/bind LEFTARROW "[^"]*"/bind LEFTARROW "+moveleft"/g' ".q3a/baseq3/q3config.cfg"
  sed -i -e 's/bind RIGHTARROW "[^"]*"/bind RIGHTARROW "+moveright"/g' ".q3a/baseq3/q3config.cfg"
else
  echo "[ERROR] Couldn't detect controller selection result. RESULT CODE: ${SELECTEDOPTION}" > "${RUNTIME_LOG_PATH}/quake3arena.log"
  echo -n 1 > "/data/power/disable"
  exit 1
fi

HOME="/var/volatile/launchtmp" LD_LIBRARY_PATH="/media/project_eris/lib" SDL_GAMECONTROLLERCONFIG="$(cat ${PROJECT_ERIS_PATH}/etc/boot_menu/gamecontrollerdb.txt)" ./ioquake3.arm &> "${RUNTIME_LOG_PATH}/quake3arena.log"
echo -n 1 > "/data/power/disable"
echo "launch_StockUI" > "/tmp/launchfilecommand"