 ioquake3 is a free software first person shooter engine based on
 the Quake 3: Arena and Quake 3: Team Arena source code. The source
 code is licensed under the GPL version 2, and was first released 
 under that license by id software on August 20th, 2005. Since then, 
 we have been cleaning it up, fixing bugs, and adding features. 
 Our permanent goal is to create the open source Quake 3 distribution 
 upon which people base their games, ports to new platforms, and other projects. 
 We have developed the perfect version of the engine for playing Quake 3: Arena, 
 Team Arena, and all popular mods.